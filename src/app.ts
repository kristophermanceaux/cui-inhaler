import https from 'https'
import $ from 'cheerio'

(async function main() {
  let cuiData = {}
  let cuiCategoriesPage: string = await getHtml('https://www.archives.gov/cui/registry/category-list');
  const categories = getCategories(cuiCategoriesPage);
  // categories.forEach((el) => console.log($(el).attr('href')))
  // console.log(categories.length);
  const categoryOIGroupings = getCategoryOIGrouping(cuiCategoriesPage);
  // categoryOIGroupings.forEach((el, i) => console.log($(el).html()))
  // console.log(categoryOIGroupings.length);
  
})()

function getCategoryOIGrouping(page: string) {
  return $('tbody > tr', page).map((i, el) => {
    return $('td', el)
  }).get()
    .splice(1)
}

function getCategories(page: string) {
  return $('td > ul > li > a', page).get()
}

function getHtml(url: string): Promise<string> {
  return new Promise((resolve, reject) => {
    let data = ""
    https.get(url, (res: any) => {
      res.on('data', (chunk: any) => {
        data += chunk;
      });
      res.on('end', (e: any) => {
        resolve(data);
      })
    }).on('error', (e: any) => {
      reject(e);
    })
  })
}

